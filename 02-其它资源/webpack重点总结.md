# webpack

作用：打包静态模块

安装：`npm i webpack webpack-cli`

打包命令：`npx webpack --config webpack.dev.js`

核心配置文件：webpack.config.js

* 处理js文件

配置文件API：

mode: 两种模式（prod和dev）

entry: 入口文件（指定打包起点）

output: 出口配置 (打包文件输出位置和命名)

* loader处理非js文件=》样式、字体图标等
* Plugin增强webpack功能=〉抽离c s s为单独文件、重新打包之前删除上次打包文件等

使用：1. 安装对应loader或plugin的npm包 2. 配置

* 自己搭建vue的开发环境

核心：webpack+loader+plugin