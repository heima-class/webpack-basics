const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')

console.log('绝对路径：', __dirname)
module.exports = {
  // 打包模式
  // mode: 'production',
  mode: "development",
  // 打包入口
  entry: './src/index.js',
  // 打包出口
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'js/[name]-[hash].js'
  },
  devServer: {
    // "host": "127.0.0.1", // 地址（默认值）
    "port": 10088, // 端口号
    "open": true // 启动后自动打开浏览器
  },
  optimization: {
    minimizer: [
      // 压缩css
      new OptimizeCssAssetsWebpackPlugin(),
      // 去除log日志
      new TerserWebpackPlugin({
        terserOptions: {
          compress: {
            drop_console: true
          }
        }
      })
    ]
  },
  // 通过rules配置loader使webpack支持更多文件类型的打包工作
  module: { // 处理非js模块
    rules: [ // 规则
      // 支持js中css样式的打包处理
      {
        test: /\.css|.less$/, 		// 正则测试=》测试匹配的是src下的所有样式文件
        // 匹配成功后（从后向前；从右到左）
        // 1. 先用less-loader去加载.less文件,转成css
        // 2. 先用css-loader去加载css文件
        // 3. 再用style-loader把样式以style标签的方式嵌入到html中
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            // 打包文件根目录
            options: { publicPath: '../' },
          },
          'css-loader',
          'less-loader'
        ] 	// loader
      },
      // 处理字体文件=> 拷贝了样式中的字体文件=》dist
      // {
      //   test: /\.(woff|woff2|eot|ttf|otf|svg)$/, // 测试后缀格式
      //   use: {
      //     loader: 'file-loader',
      //     options: {
      //       // [name]: 这里[]表示里面放一个占位符。name就是要处理的文件名
      //       // [ext]:  这里[]表示里面放一个占位符。ext就是要处理的文件后缀名
      //       name: '[name].[ext]', // 指定拷贝文件名字和后缀保持不变
      //       // 指定dist下拷贝位置
      //       outputPath: './assets/fonts'
      //     }
      //   }
      // },
      // 处理图片格式/字体图标格式
      {
        test: /\.(png|svg|gif|jpg|woff|woff2|eot|ttf|otf)$/,
        use: {
          loader: 'url-loader',
          options: {
            // 是否把一张图转成base64：
            // 优点：减少一次请求
            // 缺点：会增加.js文件的体积
            // 推荐设置：2kb=>不设置默认base64

            limit: 2 * 1024, // 如果图片小于2kb就转成base64,否则就直接拷贝(单位：字节)
            name: '[name].[ext]',
            outputPath: './assets/images'
          }
        }
      },
      // es6转es5
      {
        test: /\.js$/,
        exclude: /node_modules/,  // 排除目录
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name]-[hash].css',
    }),
    new HtmlWebpackPlugin({
      minify: {
        // 压缩HTML文件
        removeComments: true, // 移除HTML中的注释
        collapseWhitespace: true, // 删除空白符与换行符
        minifyCSS: true, // 压缩内联css
      },
      filename: 'index.html',
      // path.resolve()就是转成绝对路径
      template: path.resolve('./index.html'), // 指定模块的位置
    }),
    new CleanWebpackPlugin()
  ],
}