const path = require('path')
console.log('绝对路径：', __dirname)
module.exports = {
  // 打包模式
  /**
   * 1. production 生产模式
   * 2. development 开发模式
   */
  // mode: "production",
  mode: "development",
  // devtool: "source-map",
  devtool: "inline-source-map",
  // 打包入口
  entry: './src/mian/test.js',
  // 打包的出口
  output: {
    "path": path.join(__dirname, '/build'),
    "filename": "dev-imap.js"
  }
}