/**
 * 程序的入口（main）/webpack默认打包入口
 */
import { updateDom } from './tool'
// 引入css样式
// import './css/style.css'
import './css/index.less'

// es6
let abc = 123
const fnc = () => {
  console.log(abc)
}

console.log(456000)
console.warn(789)

updateDom('app', 'index.html')  