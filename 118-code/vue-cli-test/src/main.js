import Vue from 'vue'
import App from './App.vue'
import './css/index.less'
new Vue({
  render (h) {
    return h(App)
  },
}).$mount('#app')